package br.ucsal.testequalidade20182.locadora;


import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import br.ucsal.testequalidade20182.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20182.locadora.dominio.Cliente;
import br.ucsal.testequalidade20182.locadora.dominio.Locacao;
import br.ucsal.testequalidade20182.locadora.dominio.Modelo;
import br.ucsal.testequalidade20182.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20182.locadora.dominio.enums.SituacaoVeiculoEnum;
import br.ucsal.testequalidade20182.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20182.locadora.persistence.LocacaoDAO;
import br.ucsal.testequalidade20182.locadora.persistence.VeiculoDAO;
import junit.framework.Assert;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ ClienteDAO.class,LocacaoDAO.class, VeiculoDAO.class, LocacaoBO.class })
public class LocacaoBOUnitarioTest {
	
	
	/**
	 * Locar, para um cliente cadastrado, um veículo disponível. Método: public
	 * static Integer locarVeiculos(String cpfCliente, List<String> placas, Date
	 * dataLocacao, Integer quantidadeDiasLocacao) throws
	 * ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
	 * VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado {
	 *
	 * Observações: lembre-se de mocar os métodos necessários nas classes
	 * ClienteDAO, VeiculoDAO e LocacaoDAO.
	 * 
	 * @throws Exception
	 */
	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {
		
		Date datalocacao = new Date();
		Integer quantidadeDiasLocacao = 5;
		
		String placa = "abc-1234";
		Veiculo veiculo = new Veiculo(placa, 2019, new Modelo("Gol"), 50.20);
		veiculo.setSituacao(SituacaoVeiculoEnum.DISPONIVEL);
		List<String> placas = Arrays.asList(placa);
		List<Veiculo> veiculos = Arrays.asList(veiculo);
		
		String cpf = "1234567890";
		Cliente cliente = new Cliente(cpf, "Tiago", "12313213");
		

		PowerMockito.mockStatic(ClienteDAO.class);
		PowerMockito.when(ClienteDAO.obterPorCpf(cpf)).thenReturn(cliente);
		
		PowerMockito.mock(VeiculoDAO.class);
		PowerMockito.when(VeiculoDAO.obterPorPlaca(placa)).thenReturn(veiculo);
		
		PowerMockito.mockStatic(LocacaoDAO.class);
		
		Integer numeroContratoEsperado = 789;
		Integer numeroContratoAtual = LocacaoBO.locarVeiculos(cpf, placas, datalocacao, quantidadeDiasLocacao);
		
		Locacao locacao = new Locacao(cliente,  veiculos, datalocacao, quantidadeDiasLocacao);
		
		Assert.assertEquals(numeroContratoEsperado, numeroContratoAtual);
		
		PowerMockito.whenNew(Locacao.class).withAnyArguments().thenReturn(locacao);
		
		Whitebox.setInternalState(locacao, "numeroContrato", numeroContratoEsperado);
		
		
		
	}
}
